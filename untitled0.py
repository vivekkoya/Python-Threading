#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  1 13:43:13 2022

@author: Vivekanand Koya
"""
import threading
import time

def main():
    count1 = 0;
    count2 = 10;
    t1 = threading.Thread(); 
    t2 = threading.Thread();
    for i in range(1,11):
        t1.start()
        time.sleep(1);
    
    for i in range(10,0,-1):
        t2.start();
        time.sleep(1);